﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    private int totalCountScenes;
    private int numberOfVoce;


    private void Start()
    {
        Debug.Log("voce" + numberOfVoce);
    }

    private void Update()
    {
        GameObject[] voce = GameObject.FindGameObjectsWithTag("Voce");
        numberOfVoce = voce.Length;
        if(numberOfVoce == 0 && SceneManager.GetActiveScene().buildIndex != 0)
        {
            LoadNextScene();
        }
    }

    public void LoadNextScene()
    {
        totalCountScenes = SceneManager.sceneCountInBuildSettings;
        Debug.Log(totalCountScenes);

        int brojTrenutneScene = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(brojTrenutneScene);
        if (brojTrenutneScene == (totalCountScenes - 1))
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            SceneManager.LoadScene(brojTrenutneScene + 1);
        }

    }

    public void LoadProsluScene()
    {
        int brojTrenutneScene = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(brojTrenutneScene);
        SceneManager.LoadScene(brojTrenutneScene - 1);

    }

    public void LoadMenuScene()
    {
        SceneManager.LoadScene(0);
    }


    public void QuitGame()
    {
        Debug.Log("dode tu?");
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void ToggleMusic()
    {

    }



}
