﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Rigidbody2D rb;
    private void OnEnable()
    {
        StartCoroutine(Deactivate());
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -0.00001f);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.otherCollider)
        {
            gameObject.SetActive(false);
            StopCoroutine(Deactivate());
        }
    }

    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);

    }

}
