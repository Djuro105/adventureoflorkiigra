﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacijaHandler : MonoBehaviour
{
    private Animator animator;
    Collider2D myCollider2D;

    private string animationParameter = "AnimationParameter";

    private float hodanjePoHorizontali;

    enum AnimationStates {
        idle = 0,
        run = 1,
        jump =2
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        myCollider2D = GetComponent<Collider2D>();
    }

    void Update()
    {
        AnimationHandler();
        hodanjePoHorizontali = Input.GetAxisRaw("Horizontal");
    }

    private void AnimationHandler()
    {
        if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground"))) //nijeNaGroundu
        {
            animator.SetInteger(animationParameter, (int)AnimationStates.jump);
        }
        else if (hodanjePoHorizontali != 0)  //run
        {
            animator.SetInteger(animationParameter, (int)AnimationStates.run);
        }
        else
        {
            animator.SetInteger(animationParameter, (int)AnimationStates.idle);
        }
    }

}
