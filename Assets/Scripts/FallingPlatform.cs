﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    Collider2D myCollider;
    Animator anim;

    Vector2 pocetnaPozicija;
    public GameObject novaPlatforma;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();
        anim = GetComponent<Animator>();

        pocetnaPozicija = gameObject.transform.position;

        // postavi platformu da leti i povecaj masu tako da player ne srusi platformu svojom masom
        rigidBody.gravityScale = 0;
        rigidBody.mass = 200;
        myCollider.enabled = true;
        anim.enabled = true;

        StartCoroutine(SrusiPlatformuNakonDodira());
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SrusiPlatformuNakonDodira()
    {
        while (true)
        {
            if (myCollider.IsTouchingLayers(LayerMask.GetMask("Default")))
            {
                Debug.Log("Nesto dotaklo platformu letecu");
                yield return new WaitForSeconds(0.4f);
                anim.enabled = false;
                yield return new WaitForSeconds(0.5f);

                rigidBody.gravityScale = 4;
                myCollider.enabled = false;

                yield return new WaitForSeconds(3);
                Instantiate(gameObject, pocetnaPozicija, transform.rotation);
                //myCollider.enabled = true;
                //anim.enabled = true;
            } else
            {
                yield return null;
            }
        }
    }


}
