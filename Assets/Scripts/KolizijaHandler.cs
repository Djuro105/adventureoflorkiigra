﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KolizijaHandler : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    public GameObject explosion;
    Collider2D myCollider2D;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        myCollider2D = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Slinavac")
        {
            Debug.Log("Lorki je nastradao");
            Destroy(gameObject); // ucitaj gameover nesto
        }

        if (collision.gameObject.tag == "UgasiCirk")
        {
            Debug.Log("Lorki je ugasio cirk");
            
            rigidBody.velocity = new Vector2(0, 25);
            Instantiate(explosion, new Vector3(-16.43f, 1.36f, 0), transform.rotation);
            Destroy(GameObject.Find("Cirkular"));
        }

        if (collision.gameObject.tag == "Cirkular")
        {
            Debug.Log("Lorki je dodirnuo cirkular i umire");
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "GlavaSlinavac")
        {
            Debug.Log("Lorki je ubio slinavca");
            //Destroy(collision.gameObject);
            rigidBody.velocity = new Vector2(0, 25);
            //Explode(GameObject.Find("Slinavac").transform.position);
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(GameObject.Find("Slinavac"));
        }

        if (collision.gameObject.tag == "Ball")
        {
            Debug.Log("Ball udario lorkija");
            Destroy(gameObject); // ucitaj gameover nesto
        }

        
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Rock" && myCollider2D.IsTouchingLayers(LayerMask.GetMask("Coldet")))
        {
            Debug.Log("YEEES");
            Destroy(gameObject); // ucitaj gameover nesto
        }
    }





}
