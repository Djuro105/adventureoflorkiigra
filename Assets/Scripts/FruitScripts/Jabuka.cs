﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jabuka : MonoBehaviour
{

    private SpriteRenderer spriteRenderer;
    private AudioSource source;
    private CircleCollider2D colliderCircle;
    private bool ReadyToDestroy = false;
 
    void Start()
    {
        source = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        colliderCircle = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ReadyToDestroy == true && source.isPlaying == false)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Lorki")
        {
            ReadyToDestroy = true;
            source.Play();
            spriteRenderer.enabled = false;
            colliderCircle.enabled = false;
        }
    }


}
